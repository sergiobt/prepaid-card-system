# Prepaid Card System Project


## Authors 

- Álvaro Honrubia Genilloud
- Irene del Rincón Bello
- Sergio Barrero Torrecuadrada

## Execution

After running the Application.java programm a window with the view will appear.

On this menu screen, the user will be able to select an action to do:
- Buy a card
- Pay
- Charge money
- Change PIN
- Consult balance
- Consult movements

On each of these actions, the user will have to provide the cardnumber, of the card with which he wants to do the action, with the other required data, 
Inputs will indicate the necesary data of each action, there will be below a confirmation button of the action and a back button the menu of actions.

If there would be any error (Input or State of the application), it will be notified to the user with a message below the buttons indicating the cause of the error.

In case of success, the user will be taken to a screen where he will be notified that the actions has been done correctly, 
and any other important information that shall be notified about the action realized (ticket).