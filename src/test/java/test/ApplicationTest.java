package test;

import exceptions.*;
import org.json.simple.parser.JSONParser;
import objects.Tarjeta;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.jupiter.api.AfterAll;
import services.*;


import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ApplicationTest {
    private Servicios servicios = new Servicios();
    private JSONObject obj;
    private JSONParser jsonParser;
    private Calendar c, cad;
    private Date expirationDate, wrongExpirationDate;
    private String cardNumberA, cardNumberB, cardNumberCad ;
    private Tarjeta tarjetaA, tarjetaB, tarjetaC;
    private String[] tarjetaStrA, tarjetaStrB, tarjetaStrCad;

    @BeforeEach
    public void setting() throws WrongPinNumberLengthException, NoSuchAlgorithmException, WrongPinNumberException, ParseException, IOException {

        c = Calendar.getInstance();
        c.set((c.get(Calendar.YEAR)) + 1, (c.get(Calendar.MONTH)), (c.get(Calendar.DATE)));
        expirationDate = c.getTime();

        cad = Calendar.getInstance();
        cad.set((cad.get(Calendar.YEAR)) - 2, (cad.get(Calendar.MONTH)), (cad.get(Calendar.DATE)));
        wrongExpirationDate = cad.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String fecha = formatter.format(wrongExpirationDate);

        // Tarjeta normal
        String tarjetaCompradaA = servicios.comprarTarjeta("Jaime", "Ruiz", "1234", "1234", 100);
        tarjetaStrA = tarjetaCompradaA.split(" ");
        cardNumberA = tarjetaStrA[4];

        // Tarjeta con fallos
        String tarjetaCompradaB = servicios.comprarTarjeta("Jose", "Ramirez", "1234", "1234", 100);
        tarjetaStrB = tarjetaCompradaB.split(" ");
        cardNumberB = tarjetaStrB[4];

        // Tarjeta caducada
        String tarjetaCad = servicios.comprarTarjeta("Jaime", "Ramirez", "1234", "1234", 100);
        tarjetaStrCad = tarjetaCad.split(" ");
        cardNumberCad = tarjetaStrCad[4];

        String dir = "src/main/resources/cards/" + cardNumberCad + ".json";
        File file = new File(dir);
        FileReader reader = new FileReader(file);
        JSONParser jsonParser = new JSONParser();
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        reader.close();
        try(FileWriter writer = new FileWriter(dir)){
            obj.put("expirationDate", fecha);
            writer.write(obj.toJSONString());
            writer.flush();
        }

        // Tarjeta normal
        tarjetaA = new Tarjeta(cardNumberA, "Jaime", "Ruiz", "1234", 110, expirationDate);
        // Tarjeta con fallos
        tarjetaB = new Tarjeta("00000000000", "Jose", "Ramirez", "1234", 110, expirationDate);
        // Tarjeta caducada
        tarjetaC = new Tarjeta(cardNumberCad, "Jose", "Ramirez", "1234", 110, wrongExpirationDate);

    }

    @Test
    //Este test comprueba si se ha comprado la tarjeta y generado su informacion correctamente
    public void testBuyCard() throws Exception{

        assertTrue(tarjetaA.getCardNumber().matches("^[0-9]{12}$"));
        assertEquals("Jaime", tarjetaStrA[1]);
        assertTrue(tarjetaA.getSurname().contains("Ruiz"));
        Float amount= (float)100;
        assertTrue(tarjetaStrA[6].contains( String.valueOf(amount)));
    }
    @Test
    //Este test comprueba que comprar la tarjeta da un error al no rellenar alguno de los campos necesarios de forma correcta
    public void testBuyViewArgumentsError() throws Exception{

        assertThrows(WrongPinNumberException.class,() -> servicios.comprarTarjeta("Jaime", "Ruiz", "1235", "1234", 100));
        assertThrows(WrongPinNumberException.class,() -> servicios.comprarTarjeta("Jaime", "Ruiz", "1234", "1235", 100));
        assertThrows(WrongPinNumberLengthException.class,() -> servicios.comprarTarjeta("Jaime", "Ruiz", "123432", "1234", 100));
        assertThrows(WrongPinNumberLengthException.class,() -> servicios.comprarTarjeta("Jaime", "Ruiz", "1234", "123423", 100));
        assertThrows(WrongPinNumberLengthException.class,() -> servicios.comprarTarjeta("Jaime", "Ruiz", "12", "1234", 100));
        assertThrows(WrongPinNumberLengthException.class,() -> servicios.comprarTarjeta("Jaime", "Ruiz", "1234", "12", 100));
    }

    @Test
    // Test
    public void testIngresarDinero () throws Exception {

        String resultado = servicios.ingresarDinero(tarjetaA.getCardNumber(), "1234", 100);
        String[] ingreso = resultado.split(" ");
        assertEquals("100.0€",  ingreso[3]); // Comprobamos que el ingreso sea mayor que 0
        assertEquals("200.0€",  ingreso[10]); // Comprobamos que el balance sea mayor que 0
    }

    @Test
    public void testIngresarDineroError () throws Exception {

        assertThrows(WrongCardNumberLengthException.class, () -> servicios.ingresarDinero(tarjetaB.getCardNumber(), "1234", 100));
        assertThrows(WrongPinNumberLengthException.class, () -> servicios.ingresarDinero(tarjetaA.getCardNumber(), "12345", 100));
        assertThrows(ExpirationDateException.class, () -> servicios.ingresarDinero(tarjetaC.getCardNumber(), "1234", 100));
        assertThrows(WrongPinNumberException.class, () -> servicios.ingresarDinero(tarjetaA.getCardNumber(), "4231", 100));
        assertThrows(WrongAmountMoneyException.class, () -> servicios.ingresarDinero(tarjetaA.getCardNumber(), "1234", 0));
    }


    @Test
    public void testPago () throws Exception {

        String resultado = servicios.payService(tarjetaA.getCardNumber(), "1234", 100);
        String[] pago = resultado.split(" ");
        assertEquals("100.0€",  pago[3]); // Comprobamos que el ingreso sea mayor que 0
        assertEquals("0.0€",  pago[9]); // Comprobamos que el balance sea mayor que 0
    }

    @Test
    public void testPagoError () throws Exception {

        assertThrows(WrongCardNumberLengthException.class, () -> servicios.payService(tarjetaB.getCardNumber(), "1234", 100));
        assertThrows(WrongPinNumberLengthException.class, () -> servicios.payService(tarjetaA.getCardNumber(), "12345", 100));
        assertThrows(ExpirationDateException.class, () -> servicios.payService(tarjetaC.getCardNumber(), "1234", 100));
        assertThrows(WrongPinNumberException.class, () -> servicios.payService(tarjetaA.getCardNumber(), "4231", 100));
        assertThrows(WrongAmountMoneyException.class, () -> servicios.payService(tarjetaA.getCardNumber(), tarjetaA.getPinNumber(), 1200));
    }

    @Test
    public void testCambioPin () throws Exception {

        String resultado = servicios.changePinNumber(tarjetaA.getCardNumber(), "1234", "4321");
        String dir = "src/main/resources/cards/" + tarjetaA.getCardNumber() + ".json";
        File file = new File(dir);
        FileReader reader = new FileReader(file);
        jsonParser = new JSONParser();
        obj = (JSONObject) jsonParser.parse(reader);
        reader.close();
        obj.get("pin");
        assertEquals((String) obj.get("pinNumber"), hashPin("4321")); // Comprobamos que el cambio de pin se ha hecho correctmente
    }

    @Test
    public void testCambioPinError () throws Exception {

        assertThrows(WrongCardNumberLengthException.class, () -> servicios.changePinNumber(tarjetaB.getCardNumber(), "1234", "4321"));
        assertThrows(WrongPinNumberLengthException.class, () -> servicios.changePinNumber(tarjetaA.getCardNumber(), "1234", "12345"));
        assertThrows(WrongPinNumberLengthException.class, () -> servicios.changePinNumber(tarjetaA.getCardNumber(), "12345", "12345"));
        assertThrows(ExpirationDateException.class, () -> servicios.changePinNumber(tarjetaC.getCardNumber(), "1234", "4321"));
        assertThrows(WrongPinNumberException.class, () -> servicios.changePinNumber(tarjetaA.getCardNumber(), "1235", "4321"));
        assertThrows(WrongPinNumberException.class, () -> servicios.changePinNumber(tarjetaA.getCardNumber(), tarjetaA.getPinNumber(), "1234"));
    }


    @Test
    public void testConsultarSaldo () throws Exception {

        String resultado = servicios.consultarSaldo(tarjetaA.getCardNumber(), "1234");
        String[] saldo = resultado.split(" ");
        assertEquals("100.0€",  saldo[7]); // Comprobamos que el balance sea el mismo
    }


    @Test
    public void testConsultarSaldoError () throws Exception{

        assertThrows(WrongCardNumberLengthException.class, () -> servicios.consultarSaldo(tarjetaB.getCardNumber(), "1234"));
        assertThrows(ExpirationDateException.class, () -> servicios.consultarSaldo(tarjetaC.getCardNumber(), "1234"));
        assertThrows(WrongPinNumberException.class, () -> servicios.consultarSaldo(tarjetaA.getCardNumber(), "1235"));

    }

    @Test
    //Este test comprueba los movimientos de una tarjeta y que se haya generado su informacion correctamente
    public void testGetMovements() throws Exception{

        assertTrue(tarjetaA.getCardNumber().matches("^[0-9]{12}$"));
        String result = servicios.consultaMovimientos(tarjetaA.getCardNumber(), "1234");
        String[] resultSplit = result.split(" ");
        //fecha:
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
        //comprobacion de fecha:
        assertEquals(formatter.format(date), resultSplit[10]);
        //comprobacion de valor:
        assertEquals("100.0€", resultSplit[12]);
        //rellenamos movimientos:
        for(int i = 0; i <= 5; i++) {
            servicios.ingresarDinero(tarjetaA.getCardNumber(), "1234",50);
        }
        String result2 = servicios.consultaMovimientos(tarjetaA.getCardNumber(), "1234");
        String[] resultSplit2 = result2.split(" ");
        //comprobacion de fechas:
        assertEquals(formatter.format(date), resultSplit2[10]);
        assertEquals(formatter.format(date), resultSplit2[16]);
        assertEquals(formatter.format(date), resultSplit2[22]);
        assertEquals(formatter.format(date), resultSplit2[28]);
        //comprobacion de valores:
        assertEquals("50.0€", resultSplit2[12]);
        assertEquals("50.0€", resultSplit2[18]);
        assertEquals("50.0€", resultSplit2[24]);
        assertEquals("50.0€", resultSplit2[30]);
    }

    @Test
    //Este test comprueba que al obtener los movimientos da un error
    public void testMovementsViewArgumentsError() throws Exception{
        assertThrows(WrongCardNumberLengthException.class,() -> servicios.consultaMovimientos(tarjetaB.getCardNumber(), "1234"));
        assertThrows(WrongPinNumberLengthException.class,() -> servicios.consultaMovimientos(tarjetaA.getCardNumber(), "12"));
        assertThrows(WrongPinNumberException.class,() -> servicios.consultaMovimientos(tarjetaA.getCardNumber(), "1239"));
        assertThrows(java.io.IOException.class,() -> servicios.consultaMovimientos("999999999999", "1234"));
    }

    //Funcion para cifrar el numero pin
    //Se le pasa como parametro un pin y devuelve el pin cifrado como otro string
    private String hashPin(String pin) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest pinDigested = MessageDigest.getInstance("SHA-256");
        byte[] pinConvertido = pinDigested.digest(pin.getBytes("UTF-8"));

        StringBuilder hexString;
        hexString = new StringBuilder();
        for (int i = 0; i < pinConvertido.length; i++) {
            String hex = Integer.toHexString(0xff & pinConvertido[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
