package controllers;

import services.Servicios;

public class Controller {
    private final Servicios model;

    public Controller(Servicios model){
        this.model =model;
    }

    public String buyCardController (String name, String surname, String pinNumberFirst, String pinNumberSecond, float amount){
        String result="";
        try{
            result = model.comprarTarjeta(name,surname,pinNumberFirst,pinNumberSecond,amount);
        } catch (Exception e){
            result = e.getMessage();
        }
        return result;
    }

    public String paymentController(String cardNumber, String pinCard, float amountToPay){
        String result = "";
        try {
            result = model.payService(cardNumber, pinCard ,amountToPay);
        } catch (Exception e){
            result =  e.getMessage();
        }
        return result;
    }

    public String chargeController(String cardNumber, String pinNumber, float amountToCharge){
        String result = "";
        try {
            result = model.ingresarDinero(cardNumber, pinNumber, amountToCharge);
        } catch (Exception e){
            result = e.getMessage();
        }
        return result;
    }

    public String changePinNumberController(String cardNumber, String oldPin, String newPin){
        String result = "";
        try {
            result = model.changePinNumber(cardNumber, oldPin, newPin);
        } catch (Exception e){
            result = e.getMessage();
        }
        return result;
    }

    public String balanceController(String cardNumber, String pinNumber){
        String result = "";
        try {
            result = model.consultarSaldo(cardNumber, pinNumber);
        } catch (Exception e){
            result= e.getMessage();
        }
        return result;
    }

    public String movementsController(String cardNumber, String pinNumber){
        String result = "";
        try {
            result = model.consultaMovimientos(cardNumber, pinNumber);
        } catch (Exception e){
            result= e.getMessage();
        }
        return result;
    }
}