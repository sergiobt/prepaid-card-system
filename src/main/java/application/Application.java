package application;


import controllers.Controller;
import services.Servicios;
import views.ViewImpl;

public class Application {
    public static void main(String[] args) {
        Servicios servicios = new Servicios();
        Controller controller = new Controller(servicios);
        ViewImpl view = new ViewImpl(controller);
        view.runApplication();
    }
}
