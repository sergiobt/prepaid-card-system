package views;

import controllers.Controller;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class ViewImpl extends JFrame {
    private JLabel bienvenido = new JLabel("Welcome to Prepaid Card System");
    private JLabel comprarTarjetaText = new JLabel("Buy a new card");
    private JLabel pagoText = new JLabel("Payment");
    private JLabel ingresarDineroText = new JLabel("Charge money");
    private JLabel cambioPinText = new JLabel("Change PIN");
    private JLabel consultarSaldoText = new JLabel("Consult balance");
    private JLabel consultarMovimientosText = new JLabel("Consult movements");
    private JButton ingresarDinero = new JButton("Charge Money");
    private JButton comprarTarjeta = new JButton("Buy New Card");
    private JButton cambiarPin = new JButton("Change PIN");
    private JButton pago = new JButton("Payment");
    private JButton consultarCuenta = new JButton("Consult Balance");
    private JButton consultarMovimientos = new JButton("Consult Movements");
    private JButton register = new JButton("Register Card");
    private JButton payment = new JButton("Pay");
    private JButton charge = new JButton("Charge");
    private JButton change = new JButton("Change PIN");
    private JButton balance = new JButton("Get Balance");
    private JButton movement = new JButton("Get Movements");
    private JButton menu = new JButton("Back");
    private JButton menuPayment = new JButton("Back");
    private JButton menuCharge = new JButton("Back");
    private JButton menuChange = new JButton("Back");
    private JButton menuBalance = new JButton("Back");
    private JButton menuMovement = new JButton("Back");
    private JButton menuticket = new JButton("Back");
    JTextField nameField = new JTextField(10);
    JTextField surnameField = new JTextField(10);
    JTextField pinField = new JTextField(4);
    JTextField pin2Field = new JTextField(4);
    JTextField pinCharge = new JTextField(4);
    JTextField pinPayment = new JTextField(4);
    JTextField oldPin = new JTextField(4);
    JTextField newPin = new JTextField(4);
    JTextField balancePin = new JTextField(4);
    JTextField movementPin = new JTextField(4);
    JTextField amountField = new JTextField(4);
    JTextField amountPayment = new JTextField(4);
    JTextField amountCharge = new JTextField(4);
    JTextField cardNumberPayment = new JTextField(12);
    JTextField cardNumberCharge = new JTextField(12);
    JTextField cardNumberChange = new JTextField(12);
    JTextField cardNumberBalance = new JTextField(12);
    JTextField cardNumberMovement = new JTextField(12);
    private JLabel errorTextBuy = new JLabel("");
    private JLabel errorTextPayment = new JLabel("");
    private JLabel errorTextCharge = new JLabel("");
    private JLabel errorTextChangePin = new JLabel("");
    private JLabel errorTextBalance = new JLabel("");
    private JLabel errorTextMovement = new JLabel("");
    private JLabel ticket = new JLabel("");
    private JPanel layout = new JPanel();
    private CardLayout cardLayout = new CardLayout();

    public ViewImpl(Controller controller) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        layout.setLayout(cardLayout);
        layout.add(main(), "1");
        layout.add(buy(), "2");
        layout.add(payment(), "3");
        layout.add(charge(), "4");
        layout.add(changePIN(), "5");
        layout.add(getBalance(), "6");
        layout.add(movements(), "7");
        layout.add(tickets(), "8");

        cardLayout.show(layout, "1");

        menu.addActionListener(actionEvent -> {
            cardLayout.show(layout, "1");
            registerReset();
        });
        menuPayment.addActionListener(actionEvent -> {
            cardLayout.show(layout, "1");
            paymentReset();
        });
        menuCharge.addActionListener(actionEvent -> {
            cardLayout.show(layout, "1");
            chargeReset();
        });
        menuChange.addActionListener(actionEvent -> {
            cardLayout.show(layout, "1");
            changePinReset();
        });
        menuBalance.addActionListener(actionEvent -> {
            cardLayout.show(layout, "1");
            balanceReset();
        });
        menuMovement.addActionListener(actionEvent -> {
            cardLayout.show(layout, "1");
            movementReset();

        });
        menuticket.addActionListener(actionEvent -> {
            cardLayout.show(layout, "1");
            ticket.setText("");
        });

        comprarTarjeta.addActionListener(actionEvent -> cardLayout.show(layout, "2"));
        pago.addActionListener(actionEvent -> cardLayout.show(layout, "3"));
        ingresarDinero.addActionListener(actionEvent -> cardLayout.show(layout, "4"));
        cambiarPin.addActionListener(actionEvent -> cardLayout.show(layout, "5"));
        consultarCuenta.addActionListener(actionEvent -> cardLayout.show(layout, "6"));
        consultarMovimientos.addActionListener(actionEvent -> cardLayout.show(layout, "7"));

        register.addActionListener(actionEvent -> {
            try {
                if (nameField != null && !nameField.getText().isEmpty() && surnameField != null && !surnameField.getText().isEmpty()
                        && pinField != null && !pinField.getText().isEmpty() && pin2Field != null && !pin2Field.getText().isEmpty()
                        && amountField != null && !amountField.getText().isEmpty()) {
                    String amountText = amountField.getText();
                    if (!amountText.contains(",")) {
                        if (amountText.matches("^[0-9]+([.][0-9]{1,2})?$")) {
                            if (pinField.getText().matches("^[0-9]+$") && pin2Field.getText().matches("^[0-9]+$")) {
                                float amount = Float.parseFloat(amountText);
                                String result = controller.buyCardController(nameField.getText(), surnameField.getText(), pinField.getText(), pin2Field.getText(), amount);
                                if (result.contains("Dear")) {
                                    ticket.setText(result);
                                    cardLayout.show(layout, "8");
                                    registerReset();
                                } else {
                                    errorTextBuy.setText(result);
                                }
                            } else {
                                errorTextBuy.setText("Introduce a numeric PIN value.");
                            }
                        } else {
                            errorTextBuy.setText("Introduce a numeric amount superior or equal to zero.");
                        }
                    } else {
                        errorTextBuy.setText("Decimals must be written with '.' instead of ','");
                    }
                } else {
                    errorTextBuy.setText("Please fill out all required fields.");
                }
            } catch (Exception ignored) {
                //catched
            }
        });

        payment.addActionListener(actionEvent -> {
            try {
                if (cardNumberPayment != null && !cardNumberPayment.getText().isEmpty() && pinPayment != null && !pinPayment.getText().isEmpty()
                        && amountPayment != null && !amountPayment.getText().isEmpty()) {
                    if (pinPayment.getText().matches("^[0-9]+$")) {
                        String amountText = amountPayment.getText();
                        if (!amountText.contains(",")) {
                            if (amountText.matches("^[0-9]+([.][0-9]{1,2})?$")) {
                                String result = controller.paymentController(cardNumberPayment.getText(), pinPayment.getText(), Float.parseFloat(amountPayment.getText()));
                                if (result.contains("Dear")) {
                                    ticket.setText(result);
                                    cardLayout.show(layout, "8");
                                    paymentReset();
                                } else {
                                    errorTextPayment.setText(result);
                                }
                            } else {
                                errorTextPayment.setText("<html><p>Introduce a numeric amount superior</p><p>or equal to zero and as much two decimals.</p></html>");
                            }
                        } else {
                            errorTextPayment.setText("Decimals must be written with '.' instead of ','");
                        }
                    } else {
                        errorTextPayment.setText("Introduce a numeric PIN value.");
                    }
                } else {
                    errorTextPayment.setText("Please fill out all required fields.");
                }
            } catch (Exception e) {
                //catched
            }
        });

        charge.addActionListener(actionEvent -> {
            try {
                if (cardNumberCharge != null && !cardNumberCharge.getText().isEmpty() && pinCharge != null && !pinCharge.getText().isEmpty()
                        && amountCharge != null && !amountCharge.getText().isEmpty()) {
                    if (pinCharge.getText().matches("^[0-9]+$")) {
                        String amountText = amountCharge.getText();
                        if (!amountText.contains(",")) {
                            if (amountText.matches("^[0-9]+([.][0-9]{1,2})?$")) {
                        String result = controller.chargeController(cardNumberCharge.getText(), pinCharge.getText(), Float.parseFloat(amountCharge.getText()));
                        if (result.contains("Dear")) {
                            ticket.setText(result);
                            cardLayout.show(layout, "8");
                            chargeReset();
                        } else {
                            errorTextCharge.setText(result);
                        }
                            } else {
                                errorTextCharge.setText("<html><p>Introduce a numeric amount superior</p><p>or equal to zero and as much two decimals.</p></html>");
                            }
                        } else {
                            errorTextCharge.setText("Decimals must be written with '.' instead of ','");
                        }
                    } else {
                        errorTextCharge.setText("Introduce a numeric PIN value.");
                    }
                } else {
                    errorTextCharge.setText("Please fill out all required fields.");
                }
            } catch (Exception e) {
                //catched
            }
        });

        change.addActionListener(actionEvent -> {
            try {
                if (cardNumberChange != null && !cardNumberChange.getText().isEmpty() && oldPin != null && !oldPin.getText().isEmpty()
                        && newPin != null && !newPin.getText().isEmpty()) {
                    if (oldPin.getText().matches("^[0-9]+$") && newPin.getText().matches("^[0-9]+$")) {
                        String result = controller.changePinNumberController(cardNumberChange.getText(), oldPin.getText(), newPin.getText());
                        if (result.contains("Dear")) {
                            ticket.setText(result);
                            cardLayout.show(layout, "8");
                            changePinReset();
                        } else {
                            errorTextChangePin.setText(result);
                        }
                    } else {
                        errorTextChangePin.setText("Introduce a numeric PIN value.");
                    }
                } else {
                    errorTextChangePin.setText("Please fill out all required fields.");
                }
            } catch (Exception e) {
                //catched
            }
        });

        balance.addActionListener(actionEvent -> {
            try {
                if (cardNumberBalance != null && !cardNumberBalance.getText().isEmpty() && balancePin != null && !balancePin.getText().isEmpty()) {
                    if (balancePin.getText().matches("^[0-9]+$")) {
                        String result = controller.balanceController(cardNumberBalance.getText(), balancePin.getText());
                        if (result.contains("Dear")) {
                            ticket.setText(result);
                            cardLayout.show(layout, "8");
                            balanceReset();
                        } else {
                            errorTextBalance.setText(result);
                        }
                    } else {
                        errorTextBalance.setText("Introduce a numeric PIN value.");
                    }
                } else {
                    errorTextBalance.setText("Please fill out all required fields.");
                }
            } catch (Exception e) {
                //catched
            }
        });

        movement.addActionListener(actionEvent -> {
            try {
                if (cardNumberMovement != null && !cardNumberMovement.getText().isEmpty() && movementPin != null && !movementPin.getText().isEmpty()) {
                    if (movementPin.getText().matches("^[0-9]+$")) {
                        String result = controller.movementsController(cardNumberMovement.getText(), movementPin.getText());
                        if (result.contains("Dear")) {
                            ticket.setText(result);
                            cardLayout.show(layout, "8");
                            movementReset();
                        } else {
                            errorTextMovement.setText(result);
                        }
                    } else {
                        errorTextMovement.setText("Introduce a numeric PIN value.");
                    }
                } else {
                    errorTextMovement.setText("Please fill out all required fields.");
                }
            } catch (Exception e) {
                //catched
            }
        });
    }

    private JPanel main() {
        JPanel principalPanel = new JPanel();
        principalPanel.setLayout(new BorderLayout(10, 10));

        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();

        panelaux1.add(bienvenido);
        panelaux2.add(comprarTarjeta);
        panelaux2.add(pago);
        panelaux2.add(ingresarDinero);
        panelaux2.add(cambiarPin);
        panelaux2.add(consultarCuenta);
        panelaux2.add(consultarMovimientos);

        // arriba, izquierda, abajo, derecha
        panelaux1.setBorder(new EmptyBorder(20, 0, 0, 0));
        panelaux2.setBorder(new EmptyBorder(0, 20, 0, 20));

        principalPanel.add(panelaux1, BorderLayout.NORTH);
        principalPanel.add(panelaux2, BorderLayout.CENTER);

        return principalPanel;
    }

    private JPanel buy() {
        JPanel principalPanel = new JPanel();

        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();
        JPanel panelaux3 = new JPanel();
        JPanel panelaux4 = new JPanel();
        JPanel panelaux5 = new JPanel();

        JLabel name = new JLabel("Name:");
        JLabel surname = new JLabel("Surname:");
        JLabel pin = new JLabel("PIN:");
        JLabel pin2 = new JLabel("Reintroduce PIN:");
        JLabel amount = new JLabel("Initial Amount");

        panelaux4.add(comprarTarjetaText);
        panelaux1.add(name);
        panelaux1.add(nameField);
        panelaux1.add(surname);
        panelaux1.add(surnameField);
        panelaux2.add(pin);
        panelaux2.add(pinField);
        panelaux2.add(pin2);
        panelaux2.add(pin2Field);
        panelaux2.add(amount);
        panelaux2.add(amountField);
        panelaux3.add(register);
        panelaux3.add(menu);
        panelaux5.add(errorTextBuy);

        // arriba, izquierda, abajo, derecha
        panelaux4.setBorder(new EmptyBorder(0, 30, 0, 30));
        panelaux3.setBorder(new EmptyBorder(0, 50, 0, 50));

        principalPanel.add(panelaux4, BorderLayout.NORTH);
        principalPanel.add(panelaux1, BorderLayout.CENTER);
        principalPanel.add(panelaux2, BorderLayout.CENTER);
        principalPanel.add(panelaux3, BorderLayout.SOUTH);
        principalPanel.add(panelaux5, BorderLayout.SOUTH);

        return principalPanel;
    }

    private JPanel payment() {
        JPanel principalPanel = new JPanel();
        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();
        JPanel panelaux3 = new JPanel();
        JPanel panelaux4 = new JPanel();
        JPanel panelaux5 = new JPanel();

        JLabel cardNumber = new JLabel("Card Number:");
        JLabel pin = new JLabel("PIN:");
        JLabel amount = new JLabel("Amount:");

        panelaux4.add(pagoText);
        panelaux1.add(cardNumber);
        panelaux1.add(cardNumberPayment);
        panelaux1.add(pin);
        panelaux1.add(pinPayment);
        panelaux2.add(amount);
        panelaux2.add(amountPayment);
        panelaux3.add(payment);
        panelaux3.add(menuPayment);
        panelaux5.add(errorTextPayment);

        // arriba, izquierda, abajo, derecha
        panelaux4.setBorder(new EmptyBorder(0, 40, 0, 40));
        panelaux3.setBorder(new EmptyBorder(0, 80, 0, 80));

        principalPanel.add(panelaux4, BorderLayout.NORTH);
        principalPanel.add(panelaux1, BorderLayout.CENTER);
        principalPanel.add(panelaux2, BorderLayout.CENTER);
        principalPanel.add(panelaux3, BorderLayout.SOUTH);
        principalPanel.add(panelaux5, BorderLayout.SOUTH);
        return principalPanel;
    }

    private JPanel charge() {
        JPanel principalPanel = new JPanel();
        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();
        JPanel panelaux3 = new JPanel();
        JPanel panelaux4 = new JPanel();
        JPanel panelaux5 = new JPanel();

        JLabel cardNumber = new JLabel("Card Number:");
        JLabel pin = new JLabel("PIN:");
        JLabel amount = new JLabel("Amount:");

        panelaux4.add(ingresarDineroText);
        panelaux1.add(cardNumber);
        panelaux1.add(cardNumberCharge);
        panelaux1.add(pin);
        panelaux1.add(pinCharge);
        panelaux2.add(amount);
        panelaux2.add(amountCharge);
        panelaux3.add(charge);
        panelaux3.add(menuCharge);
        panelaux5.add(errorTextCharge);

        // arriba, izquierda, abajo, derecha
        panelaux4.setBorder(new EmptyBorder(0, 30, 0, 30));
        panelaux3.setBorder(new EmptyBorder(0, 80, 0, 80));

        principalPanel.add(panelaux4, BorderLayout.NORTH);
        principalPanel.add(panelaux1, BorderLayout.CENTER);
        principalPanel.add(panelaux2, BorderLayout.CENTER);
        principalPanel.add(panelaux3, BorderLayout.SOUTH);
        principalPanel.add(panelaux5, BorderLayout.SOUTH);
        return principalPanel;
    }

    private JPanel changePIN() {
        JPanel principalPanel = new JPanel();
        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();
        JPanel panelaux3 = new JPanel();
        JPanel panelaux4 = new JPanel();
        JPanel panelaux5 = new JPanel();

        JLabel cardNumber = new JLabel("Card Number:");
        JLabel pinOld = new JLabel("Old PIN:");
        JLabel pin = new JLabel("New PIN:");

        panelaux4.add(cambioPinText);
        panelaux1.add(cardNumber);
        panelaux1.add(cardNumberChange);
        panelaux2.add(pinOld);
        panelaux2.add(oldPin);
        panelaux2.add(pin);
        panelaux2.add(newPin);
        panelaux3.add(change);
        panelaux3.add(menuChange);
        panelaux5.add(errorTextChangePin);

        // arriba, izquierda, abajo, derecha
        panelaux4.setBorder(new EmptyBorder(0, 50, 0, 50));
        panelaux1.setBorder(new EmptyBorder(0, 20, 0, 20));
        panelaux2.setBorder(new EmptyBorder(0, 30, 0, 30));
        panelaux3.setBorder(new EmptyBorder(0, 80, 0, 80));

        principalPanel.add(panelaux4, BorderLayout.NORTH);
        principalPanel.add(panelaux1, BorderLayout.CENTER);
        principalPanel.add(panelaux2, BorderLayout.CENTER);
        principalPanel.add(panelaux3, BorderLayout.SOUTH);
        principalPanel.add(panelaux5, BorderLayout.SOUTH);
        return principalPanel;
    }

    private JPanel getBalance() {
        JPanel principalPanel = new JPanel();
        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();
        JPanel panelaux3 = new JPanel();
        JPanel panelaux4 = new JPanel();
        JPanel panelaux5 = new JPanel();

        JLabel cardNumber = new JLabel("Card Number:");
        JLabel pin = new JLabel("PIN:");

        panelaux4.add(consultarSaldoText);
        panelaux1.add(cardNumber);
        panelaux1.add(cardNumberBalance);
        panelaux2.add(pin);
        panelaux2.add(balancePin);
        panelaux3.add(balance);
        panelaux3.add(menuBalance);
        panelaux5.add(errorTextBalance);

        // arriba, izquierda, abajo, derecha
        panelaux4.setBorder(new EmptyBorder(0, 60, 0, 60));
        panelaux3.setBorder(new EmptyBorder(0, 80, 0, 80));

        principalPanel.add(panelaux4, BorderLayout.NORTH);
        principalPanel.add(panelaux1, BorderLayout.CENTER);
        principalPanel.add(panelaux2, BorderLayout.CENTER);
        principalPanel.add(panelaux3, BorderLayout.SOUTH);
        principalPanel.add(panelaux5, BorderLayout.SOUTH);
        return principalPanel;
    }

    private JPanel movements() {
        JPanel principalPanel = new JPanel();
        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();
        JPanel panelaux3 = new JPanel();
        JPanel panelaux4 = new JPanel();
        JPanel panelaux5 = new JPanel();

        JLabel cardNumber = new JLabel("Card Number:");
        JLabel pin = new JLabel("PIN:");

        panelaux4.add(consultarMovimientosText);
        panelaux1.add(cardNumber);
        panelaux1.add(cardNumberMovement);
        panelaux2.add(pin);
        panelaux2.add(movementPin);
        panelaux3.add(movement);
        panelaux3.add(menuMovement);
        panelaux5.add(errorTextMovement);

        // arriba, izquierda, abajo, derecha
        panelaux4.setBorder(new EmptyBorder(0, 60, 0, 60));
        panelaux3.setBorder(new EmptyBorder(0, 80, 0, 80));

        principalPanel.add(panelaux4, BorderLayout.NORTH);
        principalPanel.add(panelaux1, BorderLayout.CENTER);
        principalPanel.add(panelaux2, BorderLayout.CENTER);
        principalPanel.add(panelaux3, BorderLayout.SOUTH);
        principalPanel.add(panelaux5, BorderLayout.SOUTH);
        return principalPanel;
    }

    private JPanel tickets() {
        JPanel principalPanel = new JPanel();
        JPanel panelaux1 = new JPanel();
        JPanel panelaux2 = new JPanel();
        panelaux1.setBorder(new EmptyBorder(0, 100, 0, 100));
        panelaux1.add(ticket);
        panelaux2.add(menuticket);
        principalPanel.add(panelaux1, BorderLayout.CENTER);
        principalPanel.add(panelaux2, BorderLayout.SOUTH);
        return principalPanel;
    }

    public void registerReset() {
        nameField.setText("");
        surnameField.setText("");
        pinField.setText("");
        pin2Field.setText("");
        amountField.setText("");
        errorTextBuy.setText("");
    }

    private void paymentReset() {
        cardNumberPayment.setText("");
        pinPayment.setText("");
        amountPayment.setText("");
        errorTextPayment.setText("");
    }

    private void chargeReset() {
        cardNumberCharge.setText("");
        pinCharge.setText("");
        amountCharge.setText("");
        errorTextCharge.setText("");
    }

    private void changePinReset() {
        cardNumberChange.setText("");
        oldPin.setText("");
        newPin.setText("");
        errorTextChangePin.setText("");
    }

    private void balanceReset() {
        cardNumberBalance.setText("");
        balancePin.setText("");
        errorTextBalance.setText("");
    }

    private void movementReset() {
        cardNumberMovement.setText("");
        movementPin.setText("");
        errorTextMovement.setText("");
    }

    public void runApplication() {
        add(layout);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setSize(450, 235);
        setResizable(false);
    }
}
