package services;

import exceptions.*;
import objects.Movimiento;
import objects.Tarjeta;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Calendar;
import java.security.MessageDigest;


public class Servicios {
    //Servicio de compra de tarjeta:
    public String comprarTarjeta(String name, String surname, String pinNumberFirst, String pinNumberSecond, float amount)
            throws WrongPinNumberLengthException, WrongPinNumberException, IOException, org.json.simple.parser.ParseException, NoSuchAlgorithmException {
        String result = "";
        String cardNumber = getNextCardNumber(getLastID());

        //Se asigna una fecha de caducidad para un año despues de la fecha de compra de tarjeta:
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        c.set((c.get(Calendar.YEAR)) + 1, (c.get(Calendar.MONTH)), (c.get(Calendar.DATE)));
        Date expirationDate = c.getTime();

        //Comprobamos la longitud de los pines.
        if ((pinNumberFirst.length() == 4) && (pinNumberSecond.length() == 4)) {
            // Comprobamos que los dos pines introducidos son iguales.
            if (pinNumberFirst.equals(pinNumberSecond)) {
                // Creamos la tarjeta con el pin cifrado.
                Tarjeta nuevaTarjeta = new Tarjeta(cardNumber, name, surname, hashPin(pinNumberFirst), amount, expirationDate);
                // Registramos la tarjeta en el sistema.
                addTarjeta(nuevaTarjeta);
                // Registramos el ultimo card number creado.
                setLastID(nuevaTarjeta.getCardNumber());

                // Registramos el movimiento en el sistema.
                Movimiento movimiento = new Movimiento(date, amount, nuevaTarjeta.getCardNumber());
                addMovimiento(movimiento, nuevaTarjeta.getCardNumber());

                result = "<html><p>Dear " + nuevaTarjeta.getName() + " " + nuevaTarjeta.getSurname() + ",</p>"
                        + "<p>Card Number: " + nuevaTarjeta.getCardNumber() + " </p>"
                        + "<p>Balance: " + nuevaTarjeta.getAmount() + "€ </p>"
                        + "<p>Thanks for using our system</p></html> ";
            } else {
                throw new WrongPinNumberException("Both PIN number must be equal.");
            }
        } else {
            throw new WrongPinNumberLengthException("The PIN number must have 4 digits.");
        }
        return result;
    }

    //Servicio de pago:
    public String payService(String cardNumber, String pinCard, float amountToPay)
            throws WrongPinNumberException, WrongAmountMoneyException, ExpirationDateException, WrongCardNumberLengthException,
            IOException, NoSuchAlgorithmException, org.json.simple.parser.ParseException, ParseException, WrongPinNumberLengthException {
        //Se muestran en primer lugar los mensajes de error para simplificar la complejidad de esta funcion:
        String mensaje;
        //se le asigna la fecha del pago:
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();

        // Comprobamos que el numero de tarjeta tenga 12 digitos
        if (cardNumber.length() != 12) {
            throw new WrongCardNumberLengthException("The length of the card number must be 12 digits.");
        }
        if (pinCard.length() != 4) {
            throw new WrongPinNumberLengthException("The pin number must have 4 digits.");
        }
        Tarjeta tarjeta = getTarjeta(cardNumber); //asignar tarjeta con ese cardNumber
        // Comprobamos la caducidad de la tarjeta.
        if (date.after(tarjeta.getExpirationDate())) {
            throw new ExpirationDateException("The card has expired.");
        }
        //Comprobamos el numero PIN
        if (!tarjeta.getPinNumber().equals(hashPin(pinCard))) {
            throw new WrongPinNumberException("The pin entered is not correct.");
        }
        if (tarjeta.getAmount() < amountToPay) {
            throw new WrongAmountMoneyException("You do not have enough money to make this payment.");
        }

        //realizar el pago:
        tarjeta.setAmount((tarjeta.getAmount()) - amountToPay);
        //modificar cantidad de dinero de tarjeta:
        modificarTarjeta(tarjeta);
        //registrar movimiento en la tarjeta:
        Movimiento movimiento = new Movimiento(date, -amountToPay, tarjeta.getCardNumber());
        addMovimiento(movimiento, cardNumber);
        //operacion exitosa, mostrar mensaje:
        /*Dear $Name $Surname,
        Amount: $Amount
        Card Number: XXXX XXXX $Last 4 digits
        Balance: $Balance
        Thanks for using our system */
        mensaje = "<html><p>Dear " + tarjeta.getName() + " " + tarjeta.getSurname() + ",</p>"
                + "<p>Amount: " + amountToPay + "€ </p>"
                + "<p>Card Number: XXXX XXXX " + tarjeta.getCardNumber().substring(tarjeta.getCardNumber().length() - 4) + "</p>"
                + "<p>Balance: " + tarjeta.getAmount() + "€ </p>"
                + "<p>Thanks for using our system</p></html> ";
        return mensaje;
    }

    //Servicio de ingresar dinero:
    public String ingresarDinero(String cardNumber, String pinNumber, float amountToCharge)
            throws WrongPinNumberException, WrongCardNumberLengthException, ExpirationDateException, IOException,
            NoSuchAlgorithmException, ParseException, org.json.simple.parser.ParseException, WrongPinNumberLengthException, WrongAmountMoneyException {
        //Se muestran en primer lugar los mensajes de error para simplificar la complejidad de esta funcion:
        String result = "";
        //se le asigna la fecha del ingreso:
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();

        // Comprobamos que el numero de tarjeta tenga 12 digitos
        if (cardNumber.length() != 12) {
            throw new WrongCardNumberLengthException("The length of the card number must be 12 digits.");
        }
        if (pinNumber.length() != 4) {
            throw new WrongPinNumberLengthException("The pin number must have 4 digits.");
        }
        Tarjeta tarjeta = getTarjeta(cardNumber);
        // Comprobamos la caducidad de la tarjeta.
        if (date.after(tarjeta.getExpirationDate())) {
            throw new ExpirationDateException("The card has expired.");
        }
        // Comprobamos que el PIN sea el asociado a la tarjeta
        if (!tarjeta.getPinNumber().equals(hashPin(pinNumber))) {
            throw new WrongPinNumberException("The pin entered is not correct.");
        }
        if (amountToCharge <= 0) {
            throw new WrongAmountMoneyException("You do not have enough money to make this charge.");
        }
        // Ingresamos dinero en la tarjeta.
        tarjeta.setAmount(tarjeta.getAmount() + amountToCharge);
        modificarTarjeta(tarjeta);
        // Registramos el movimiento en el sistema
        Movimiento movimiento = new Movimiento(date, amountToCharge, tarjeta.getCardNumber());
        addMovimiento(movimiento, cardNumber);

        result = "<html><p>Dear " + tarjeta.getName() + " " + tarjeta.getSurname() + ",</p>"
                + "<p>Amount: " + amountToCharge + "€ </p>"
                + "<p>Card Number:  XXXX XXXX " + tarjeta.getCardNumber().substring(tarjeta.getCardNumber().length() - 4) + "</p>"
                + "<p>Balance: " + tarjeta.getAmount() + "€ </p>"
                + "<p>Thanks for using our system</p></html> ";
        return result;
    }

    //Servicio de cambio de pin:
    public String changePinNumber(String cardNumber, String oldPin, String newPin) throws WrongPinNumberException,
            IOException, NoSuchAlgorithmException, ParseException, org.json.simple.parser.ParseException,
            ExpirationDateException, WrongCardNumberLengthException, WrongPinNumberLengthException {
        //Se muestran en primer lugar los mensajes de error para simplificar la complejidad de esta funcion:
        String result = "";
        // Comprobamos que el numero de tarjeta tenga 12 digitos
        if (cardNumber.length() != 12) {
            throw new WrongCardNumberLengthException("The length of the card number must be 12 digits.");
        }
        if (oldPin.length() != 4 || newPin.length() != 4) {
            throw new WrongPinNumberLengthException("The pin number must have 4 digits.");
        }
        //Obtenemos la tarjeta correspondiente a ese cardNumber
        Tarjeta tarjeta = getTarjeta(cardNumber);
        // Comprobamos la caducidad de la tarjeta
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();
        if (!date.before(tarjeta.getExpirationDate())) {
            throw new ExpirationDateException("The card has expired.");
        }
        //Comprobamos el numero PIN (cifrado)
        String hashOldPin = hashPin(oldPin);
        String hashNewPin = hashPin(newPin);
        if (!hashOldPin.equals(tarjeta.getPinNumber())) {
            throw new WrongPinNumberException("The old pin is not correct.");
        }
        if (hashOldPin.equals(hashNewPin)) {
            //error pin nuevo no puede ser igual que el antiguo
            throw new WrongPinNumberException("The new pin cannot be the same as the old one.");
        }
        tarjeta.setPinNumber(hashNewPin);
        //almacenar nuevo pin en la tarjeta
        modificarTarjeta(tarjeta);

        //imprimimos ticket (opcional) para mayor congruencia con el resto de metodos
        result = "<html><p>Dear " + tarjeta.getName() + " " + tarjeta.getSurname() + ",</p>"
                + "<p>Card Number:  XXXX XXXX " + tarjeta.getCardNumber().substring(tarjeta.getCardNumber().length() - 4) + "</p>"
                + "<p>The PIN number has been changed successfully</p>"
                + "<p>Thanks for using our system</p></html> ";
        return result;
    }

    //Servicio de consulta de saldo (balance):
    public String consultarSaldo(String cardNumber, String pinNumber) throws WrongPinNumberException,
            WrongCardNumberLengthException, ExpirationDateException, IOException,
            NoSuchAlgorithmException, ParseException, org.json.simple.parser.ParseException {
        String result = "";
        //se le asigna la fecha de la consulta:
        Calendar c = Calendar.getInstance();
        Date date = c.getTime();

        // Comprobamos que el numero de tarjeta tenga 12 digitos
        if (cardNumber.length() == 12) {
            Tarjeta tarjeta = getTarjeta(cardNumber);
            // Comprobamos la caducidad de la tarjeta.
            if (date.before(tarjeta.getExpirationDate())) {

                //Comprobamos el numero PIN
                if (tarjeta.getPinNumber().equals(hashPin(pinNumber))) {
                    result = "<html><p>Dear " + tarjeta.getName() + " " + tarjeta.getSurname() + ",</p>"
                            + "<p>Card Number: XXXX XXXX " + tarjeta.getCardNumber().substring(tarjeta.getCardNumber().length() - 4) + "</p>"
                            + "<p>Balance: " + tarjeta.getAmount() + "€ </p>"
                            + "<p>Thanks for using our system</p></html> ";
                } else {
                    throw new WrongPinNumberException("The pin entered is not correct.");
                }
            } else {
                throw new ExpirationDateException("The card has expired.");
            }
        } else {
            throw new WrongCardNumberLengthException("The length of the card number must be 12 digits.");
        }
        return result;
    }

    //Servicio de consulta de movimientos:
    public String consultaMovimientos(String cardNumber, String pinNumber) throws WrongPinNumberException, WrongCardNumberLengthException,
            IOException, NoSuchAlgorithmException, ParseException, org.json.simple.parser.ParseException, WrongPinNumberLengthException {
        Tarjeta tarjeta;
        String movimientos = "";

        // Comprobamos que el numero de tarjeta tenga 12 digitos
        if (cardNumber.length() == 12) {
            if (pinNumber.length() == 4) {
                tarjeta = getTarjeta(cardNumber);
                //Ciframos el numero PIN
                if (tarjeta.getPinNumber().equals(hashPin(pinNumber))) {
                    Movimiento[] mov = getMovimientos(cardNumber);
                    StringBuilder moves = new StringBuilder("<table>");
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
                    //realizar la consulta de movimientos y mostrar mensaje:
                    /*Dear $Name $Surname,
                    Card Number XXXX XXXX $Last4Digits

                    01/02/18    13.12
                    02/02/18    23.22
                    04/03/18    65.71
                    Thanks for using our system
                     */
                    //Muestra un maximo de los 4 ultimos elementos
                    int i = mov.length - 1;
                    while ((i > mov.length - 5) && (i >= 0)) {
                        moves.append("<tr><td style='width: 60px;'> " + formatter.format(mov[i].getMoveDate()) + " </td>"
                                + "<td> " + mov[i].getMoveAmount() + "€ </td></tr>");
                        i--;
                    }
                    moves.append("</table>");

                    movimientos = "<html><p>Dear " + tarjeta.getName() + " " + tarjeta.getSurname() + ",</p>"
                            + "<p>Card Number:  XXXX XXXX " + tarjeta.getCardNumber().substring(tarjeta.getCardNumber().length() - 4) + "</p>"
                            + "<div>" + moves.toString() + "</div>"
                            + "<p>Thanks for using our system</p></html> ";
                } else {
                    throw new WrongPinNumberException("The pin entered is not correct.");
                }
            } else {
                throw new WrongPinNumberLengthException("The pin number must have 4 digits.");
            }
        } else {
            throw new WrongCardNumberLengthException("The length of the card number must be 12 digits.");
        }
        return movimientos;
    }


    //Funciones auxiliares:

    //Funcion para cifrar el numero pin
    //Se le pasa como parametro un pin y devuelve el pin cifrado como otro string
    private String hashPin(String pin) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest pinDigested = MessageDigest.getInstance("SHA-256");
        byte[] pinConvertido = pinDigested.digest(pin.getBytes("UTF-8"));

        StringBuilder hexString;
        hexString = new StringBuilder();
        for (int i = 0; i < pinConvertido.length; i++) {
            String hex = Integer.toHexString(0xff & pinConvertido[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    //Dado un numero de tarjeta, se obtiene el objeto Tarjeta correspondiente a ese numero
    private Tarjeta getTarjeta(String cardNumber) throws IOException, ParseException, org.json.simple.parser.ParseException {
        JSONParser jsonParser = new JSONParser();
        String dir = "src/main/resources/cards/" + cardNumber + ".json";
        File file = new File(dir);
        boolean exists = file.exists();
        JSONObject obj;
        if (exists) {
            FileReader reader = new FileReader(file);
            obj = (JSONObject) jsonParser.parse(reader);
            reader.close();
        } else {
            throw new IOException("The card does not exist.");
        }
        return new Tarjeta((String) obj.get("cardNumber"), (String) obj.get("name"), (String) obj.get("surname"), (String) obj.get("pinNumber"), Float.parseFloat((String) obj.get("amount")), new SimpleDateFormat("dd-MM-yyyy").parse((String) obj.get("expirationDate")));
    }

    //Dado un numero de tarjeta, devuelve la lista de movimientos que se han realizado en esa tarjeta
    private Movimiento[] getMovimientos(String cardNumber) throws IOException, ParseException, org.json.simple.parser.ParseException {
        JSONParser jsonParser = new JSONParser();
        String dir = "src/main/resources/cards/" + cardNumber + ".json";
        File file = new File(dir);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
        JSONObject obj;

        FileReader reader = new FileReader(file);
        obj = (JSONObject) jsonParser.parse(reader);
        reader.close();

        JSONArray list = (JSONArray) obj.get("movements");
        int size = list.size();
        Movimiento[] movimientos = new Movimiento[size];
        for (int i = 0; i < size; i++) {
            JSONObject mov = (JSONObject) list.get(i);
            double amount = (double) mov.get("amount");
            Movimiento movimiento = new Movimiento(formatter.parse((String) mov.get("date")), (float) amount, cardNumber);
            movimientos[i] = movimiento;
        }
        return movimientos;
    }

    //Dada una tarjeta, crea su correspondiente informacion en resources como un nuevo json
    private void addTarjeta(Tarjeta tarjeta) throws IOException {
        String dir = "src/main/resources/cards/" + tarjeta.getCardNumber() + ".json";
        JSONObject card = new JSONObject();
        card.put("cardNumber", tarjeta.getCardNumber());
        card.put("name", tarjeta.getName());
        card.put("surname", tarjeta.getSurname());
        card.put("pinNumber", tarjeta.getPinNumber());
        card.put("amount", String.valueOf(tarjeta.getAmount()));
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String fecha = formatter.format(tarjeta.getExpirationDate());
        card.put("expirationDate", fecha);
        JSONArray list = new JSONArray();
        card.put("movements", list);
        try (FileWriter writer = new FileWriter(dir)) {
            writer.write(card.toJSONString());
            writer.flush();
        }
    }

    //dada una tarjeta, realiza cambios UNICAMENTE en los campos de amount y pin
    private void modificarTarjeta(Tarjeta tarjeta) throws IOException, org.json.simple.parser.ParseException {
        JSONParser jsonParser = new JSONParser();
        String dir = "src/main/resources/cards/" + tarjeta.getCardNumber() + ".json";
        File file = new File(dir);
        JSONObject obj;

        FileReader reader = new FileReader(file);
        obj = (JSONObject) jsonParser.parse(reader);
        reader.close();
        obj.put("pinNumber", tarjeta.getPinNumber());
        obj.put("amount", String.valueOf(tarjeta.getAmount()));
        try (FileWriter writer = new FileWriter(dir)) {
            writer.write(obj.toJSONString());
            writer.flush();
        }

    }

    //dado un movimiento y un numero de tarjeta, añade el movimiento a la tarjeta con ese numero de tarjeta
    private void addMovimiento(Movimiento movimiento, String idTarjeta) throws IOException, org.json.simple.parser.ParseException {
        JSONParser jsonParser = new JSONParser();
        String dir = "src/main/resources/cards/" + idTarjeta + ".json";
        JSONObject obj;
        FileReader reader = new FileReader(dir);
        obj = (JSONObject) jsonParser.parse(reader);
        reader.close();
        JSONArray list = (JSONArray) obj.get("movements");
        JSONObject movement = new JSONObject();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy");
        String fecha = formatter.format(movimiento.getMoveDate());
        movement.put("date", fecha);
        movement.put("amount", movimiento.getMoveAmount());
        list.add(movement);
        try (FileWriter writer = new FileWriter(dir)) {
            writer.write(obj.toJSONString());
            writer.flush();
        }

    }

    //obtiene el numero de tarjeta de la ultima tarjeta que se ha creado
    private String getLastID() throws IOException, org.json.simple.parser.ParseException {
        JSONParser jsonParser = new JSONParser();
        String dir = "src/main/resources/lastCardID.json";
        FileReader reader = new FileReader(dir);
        JSONObject obj = (JSONObject) jsonParser.parse(reader);
        reader.close();
        return (String) obj.get("lastID");
    }

    //modifica el numero de tarjeta de la ultima tarjeta que se ha creado
    private String getNextCardNumber(String lastCardNumber) {
        StringBuilder nextCardNumber = new StringBuilder();
        long number = Long.parseLong(lastCardNumber);
        number++;
        String stringNumber = String.valueOf(number);

        if (stringNumber.length() == 13) {
            //Se ha excedido el limite de tarjetas. Volviendo al principio:
            stringNumber = stringNumber.substring(1);
            nextCardNumber.append(stringNumber);
        } else {
            if (stringNumber.length() < 12) {
                int ceros = 12 - stringNumber.length();
                for (int i = 0; i < ceros; i++) {
                    nextCardNumber.append("0");
                }
            }
        }
        nextCardNumber.append(String.valueOf(number));

        return nextCardNumber.toString();
    }

    private void setLastID(String lastID) throws IOException {
        String dir = "src/main/resources/lastCardID.json";
        try (FileWriter writer = new FileWriter(dir)) {
            JSONObject lastIdJSON = new JSONObject();
            lastIdJSON.put("lastID", lastID);
            writer.write(lastIdJSON.toJSONString());
            writer.flush();
        }
    }
}
