package exceptions;

public class WrongPinNumberLengthException extends Exception {
    public WrongPinNumberLengthException (String msj) {
        super(msj);
    }
}
