package exceptions;

public class WrongPinNumberException extends Exception{
    public WrongPinNumberException(String msj){
        super(msj);
    }
}