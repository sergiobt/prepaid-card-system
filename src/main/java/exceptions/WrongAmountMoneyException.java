package exceptions;

public class WrongAmountMoneyException extends Exception{
    public WrongAmountMoneyException(String msj){
        super(msj);
    }
}