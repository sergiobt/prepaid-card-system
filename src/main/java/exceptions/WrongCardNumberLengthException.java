package exceptions;

public class WrongCardNumberLengthException extends Exception{
    public WrongCardNumberLengthException(String msj) {
        super (msj);
    }
}
