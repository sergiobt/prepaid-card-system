package objects;

import java.util.Date;

public class Tarjeta {

    // Numero de la tarjeta
    private String cardNumber;
    // Nombre y apellido del usuario de la tarjeta.
    private String name;
    private String surname;
    // PIN de la tarjeta.
    private String pinNumber;
    // Cantidad de dinero que tiene la tarjeta.
    private float amount;
    // Fecha de caducidad.
    private Date expirationDate;

    public Tarjeta(String cardNumber, String name, String surname, String pinNumber, float amount, Date expirationDate) {
        this.cardNumber = cardNumber;
        this.name = name;
        this.surname = surname;
        this.pinNumber = pinNumber;
        this.amount = amount;
        this.expirationDate = expirationDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPinNumber() {
        return pinNumber;
    }

    public void setPinNumber(String pinNumber) {
        this.pinNumber = pinNumber;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

}
