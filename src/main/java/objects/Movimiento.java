package objects;
import java.util.Date;

public class Movimiento {
    //introducir dinero en la tarjeta o realizar un pago
    //fecha en la que se realiza el movimiento
    private Date moveDate;
    //importe del movimiento
    private float moveAmount;

    private String moveCard;

    public Movimiento(Date moveDate, float moveAmount, String moveCard) {
        this.moveDate = moveDate;
        this.moveAmount = moveAmount;
        this.moveCard = moveCard;
    }
    
    public Date getMoveDate() {
        return moveDate;
    }

    public float getMoveAmount() {
        return moveAmount;
    }

}
